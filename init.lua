-- forces the map into singlenode mode, don't do this if this is just a "realm".
luamap.set_singlenode()

-- local seed = math.random(2147483647)

function land_spline(x)
    if x < -2 or x > 2 then
        return x
    end
    
    local y = 0
    if x <= -1.281 then
        y = 0.31212295650596195*x^3+1.8727377390357718*x^2+4.186345395287897*x^1+1.3787234864804014
    elseif x <= -0.661 then
        y = -0.051430733255935245*x^3+0.47560090928080084*x^2+2.3966131163717788*x^1+0.6145078033832191
    elseif x <= -0.180 then
        y = -2.278394433784434*x^3+-3.9404681088672127*x^2+-0.5224085046240582*x^1+-0.028649960442863606
    elseif x <= 0.190 then
        y = 4.311264272962816*x^3+-0.3820524072236974*x^2+0.11810632167177455*x^1+0.009780929134886358
    elseif x <= 0.665 then
        y = -2.1664005801613775*x^3+3.3102165590570927*x^2+-0.5834247819215757*x^1+0.0542112323624652
    elseif x <= 1.520 then
        y = 0.9513126311062093*x^3+-2.909621297421743*x^2+3.55276739263685*x^1+-0.8626446996646525
    else
        y = -0.9919196530712304*x^3+5.951517918427383*x^2+-9.91616421545382*x^1+5.961613981767954
    end
    
    return y
end

-- main noise
local recip_factors = {371, 223, 141, 73, 32, 17} --total = 840

for i, r_fac in ipairs(recip_factors) do
    local is_eased = false
    if i <= 4 then is_eased = true end
    luamap.register_noise("terrain_o" .. i, {
        type = "2d",
        np_vals = {
            offset = 0, -- from -r_fac to +r_fac
            scale = 0.5*r_fac,
            spread = {x=r_fac, y=r_fac, z=r_fac},
            seed = 65462139,
            octaves = math.floor(math.log(r_fac, 2)),
            persist = 0.4,
            lacunarity = 2,
            flags = is_eased,
        },
    })
end

luamap.register_noise("surface_3d", {
    type = "3d",
    np_vals = {
        offset = 0,
        scale = 1,
        spread = {x=32, y=32, z=32},
        seed = 7195270625,
        octaves = 4,
        persist = 0.5,
        lacunarity = 2,
        flags = "eased",
    },
    ymin = -256,
    ymax = 512,
})

luamap.register_noise("caves_blob", {
    type = "3d",
    np_vals = {
        offset = 0,
        scale = 1,
        spread = {x=16, y=12, z=16},
        seed = 1100581792,
        octaves = 4,
        persist = 0.4,
        lacunarity = 2,
        flags = "eased",
    },
    ymax = 32,
})

luamap.register_noise("caves_blob_large", {
    type = "3d",
    np_vals = {
        offset = 0,
        scale = 1,
        spread = {x=48, y=32, z=48},
        seed = 8796758322,
        octaves = 5,
        persist = 0.35,
        lacunarity = 2,
        flags = "",
    },
    ymax = -256,
})

luamap.register_noise("caves_sheet", {
    type = "3d",
    np_vals = {
        offset = 0,
        scale = 1,
        spread = {x=192, y=192, z=192},
        seed = 968236844,
        octaves = 6,
        persist = 0.45,
        lacunarity = 2,
        flags = "",
    },
    ymax = 32
})
luamap.register_noise("caves_sheet2", {
    type = "3d",
    np_vals = {
        offset = 0,
        scale = 1,
        spread = {x=32, y=24, z=32},
        seed = 14137677187,
        octaves = 5,
        persist = 0.2,
        lacunarity = 2,
        flags = "",
    },
    ymax = 32
})

luamap.register_noise("caves_noodle1", {
    type = "3d",
    np_vals = {
        offset = 0,
        scale = 1,
        spread = {x=64, y=80, z=64},
        seed = 6579241763,
        octaves = 6,
        persist = 0.25,
        lacunarity = 2,
        flags = "",
    },
})
luamap.register_noise("caves_noodle2", {
    type = "3d",
    np_vals = {
        offset = 0,
        scale = 1,
        spread = {x=80, y=64, z=80},
        seed = 8175385003,
        octaves = 6,
        persist = 0.25,
        lacunarity = 2,
        flags = "",
    },
})

luamap.register_noise("shaper", {
    type = "2d",
    np_vals = {
        offset = 0,
        scale = 1,
        spread = {x=256, y=256, z=256},
        seed = 2136961411,
        octaves = 4,
        persist = 0.35,
        lacunarity = 4,
        flags = "",
    },
})

luamap.register_noise("amplifier", {
    type = "2d",
    np_vals = {
        offset = 0,
        scale = 1,
        spread = {x=256, y=256, z=256},
        seed = 318027505,
        octaves = 3,
        persist = 0.25,
        lacunarity = 4,
        flags = "eased",
    },
})

luamap.register_noise("ridge_factor", {
    type = "2d",
    np_vals = {
        offset = 0,
        scale = 1,
        spread = {x=384, y=384, z=384},
        seed = 1036203407,
        octaves = 7,
        persist = 0.5,
        lacunarity = 2,
        flags = "eased",
    },
})
luamap.register_noise("ridge_depth", {
    type = "2d",
    np_vals = {
        offset = -8,
        scale = 8,
        spread = {x=16, y=16, z=16},
        seed = 1700422935,
        octaves = 4,
        persist = 0.3,
        lacunarity = 2,
        flags = "eased",
    },
})

local c_air = minetest.get_content_id("air")
local c_stone = minetest.get_content_id("default:stone")
local c_sandstone = minetest.get_content_id("default:sandstone")
local c_water = minetest.get_content_id("default:water_source")

local water_level = 0

local old_logic = luamap.logic

function luamap.logic(noise_vals,x,y,z,seed,original_content)
    -- get any terrain defined in another mod
    local content = old_logic(noise_vals,x,y,z,seed,original_content)
    
    if y < water_level then
        content = c_water
    end
    local empty_content = content

    local terrain_height = 8
    for i = 1, #recip_factors do
        local name = "terrain_o" .. i
        local o_height = noise_vals[name]
        if i <= 2 then
            o_height = luamap.coserp(0, o_height, o_height / recip_factors[i]) -- smoothing at large scales
        end
        terrain_height = terrain_height + o_height
    end
    
    -- interpolate between unmodified terrain and shaped terrain, skewed towards the latter
    terrain_height = luamap.coserp(terrain_height, 1.1 * 256 * land_spline(terrain_height / 256), luamap.remap(noise_vals.shaper, -2, 2, 0.4, 1))
    
    if terrain_height > water_level then
        terrain_height = terrain_height * luamap.remap(noise_vals.amplifier, -2, 2, 0.4, 0.8)
    end
    
    -- thread-like ravines with sloping sides
    local ravine_depth = 0
    local ravine_proximity = math.abs(noise_vals.ridge_factor + 0.8)
    if ravine_proximity < 0.02 then
        ravine_depth = noise_vals.ridge_depth
    elseif ravine_proximity < 0.05 then
        ravine_depth = luamap.coserp(0, noise_vals.ridge_depth, luamap.remap(ravine_proximity, 0.02, 0.05, 1.0, 0.0))
    end
    terrain_height = terrain_height + ravine_depth
    
    -- 3d landscaping
    local density_3d = luamap.remap(y, math.min(water_level - 128, terrain_height - 64), math.min(terrain_height + 64, 192), 0, 1)
    density_3d = luamap.coserp(-2, -0.5, density_3d)
    if y < terrain_height then
        if y > terrain_height - 8 then
            if noise_vals.surface_3d > density_3d then
                content = c_stone
            end
        elseif y >= water_level - 256 then
            -- interpolate between 3d effect and 2d solid ground
            if noise_vals.surface_3d > density_3d - luamap.remap(terrain_height - y, 8, 32, 0, 1.5) then
                content = c_stone
            end
        else
            content = c_stone
        end
    end

    -- if y == math.floor(terrain_height) and content == c_stone then
    --     content = c_sandstone
    -- end

    -- caves test
    local caves_blob = false
    local caves_bloodle_factor = 0
    local caves_blob_threshold = luamap.remap(y, -1024, 64, -0.85, -1.02)
    if noise_vals.caves_blob < caves_blob_threshold then
        caves_blob = true
    else
        -- noodles near blobs try to connect better
        caves_bloodle_factor = caves_bloodle_factor + luamap.remap(noise_vals.caves_blob - caves_blob_threshold, 0, 1, 0.01, 0)
    end

    local caves_blob_large = false
    local caves_blob_large_threshold = luamap.remap(y, -2048, -256, -0.5, -0.9)
    if noise_vals.caves_blob_large < caves_blob_large_threshold then
        caves_blob_large = true
    else
        caves_bloodle_factor = caves_bloodle_factor + luamap.remap(noise_vals.caves_blob_large - caves_blob_threshold, 0, 1, 0.012, 0)
    end

    local caves_sheet = noise_vals.caves_sheet2 > -0.2
    local caves_sheet_margin = luamap.remap(y, -1024, 32, 0.014, 0.010)
    local caves_sheet_offset = luamap.remap(y, -3072, 32, -0.3, 0.2)

    local caves_noodle_margin = luamap.remap(y, -1024, 128, 0.026, 0.019) + caves_bloodle_factor
    local caves_noodle_offset = luamap.remap(y, -2048, 128, -0.15, 0.2)
    local caves_noodle = math.abs(noise_vals.caves_noodle1 - caves_noodle_offset) < caves_noodle_margin and
                         math.abs(noise_vals.caves_noodle2 - caves_noodle_offset) < caves_noodle_margin

    if content == c_stone then
        if caves_blob then
            content = c_air
        elseif caves_blob_large then
            content = c_air
        elseif caves_noodle then
            content = c_air
        elseif caves_sheet and math.abs(noise_vals.caves_sheet - caves_sheet_offset) < caves_sheet_margin then
            content = c_air
        end
    end

    return content
end
